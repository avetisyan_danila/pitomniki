module.exports = {
  stories: ['./../src/**/*.stories.js'],
  addons: [
    '@storybook/addon-notes/register',
    '@storybook/addon-viewport/register',
    '@storybook/addon-actions/register',
  ],
};
