/* eslint-disable linebreak-style */
module.exports = [
    {block: 'slider-partners', content: [
        {elem: 'inner', cls: 'container', content: [
            {block: 'swiper', cls: 'swiper-partners', content: [
                {cls: 'swiper-wrapper', content: [
                    [...new Array(12)].map(() => [
                        {elem: 'slide', cls: 'swiper-slide', content: [
                            {block: 'client-card', content: [
                                {elem: 'image', content: [
                                    {block: 'image', mods: {size: '130x80'}, cls: 'text-center', content: [
                                        {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/130x80'},
                                    ]},
                                ]},
                            ]},
                        ]},
                    ]),
                ]},
            ]},
            {elem: 'navigation', content: [
                {elem: 'buttons', content: [
                    {elem: 'button', mods: {prev: true}},
                    {elem: 'button', mods: {next: true}},
                ]},
            ]},
        ]},
    ]},
];
