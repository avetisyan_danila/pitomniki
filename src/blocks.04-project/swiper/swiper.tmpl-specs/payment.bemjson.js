/* eslint-disable linebreak-style */
module.exports = [
    {block: 'swiper', cls: 'swiper-payment', content: [
        {cls: 'swiper-wrapper', content: [
            {elem: 'slide', cls: 'swiper-slide', content: [
                {block: 'square-card', content: [
                    {elem: 'top', content: [
                        {elem: 'title', attrs: {'data-toggle': 'ellipsis'}, content: 'Банковская карта VISA или MasterCard'},
                    ]},
                    {elem: 'bottom', content: [
                        {elem: 'icon', content: [
                            {block: 'image', mods: {size: '99x25'}, content: [
                                {block: 'img', mods: {lazy: true}, src: './images/payment/visa-mastercard.png'},
                            ]},
                        ]},
                    ]},
                    {elem: 'disable', content: [
                        {elem: 'disable-text', content: [
                            {block: 'b', content: 'В разработке,'},
                            {block: 'span', content: 'но совсем скоро появится на сайте'},
                        ]},
                    ]},
                ]},
            ]},
            {elem: 'slide', cls: 'swiper-slide', content: [
                {block: 'square-card', content: [
                    {elem: 'top', content: [
                        {elem: 'title', attrs: {'data-toggle': 'ellipsis'}, content: 'Расчёт наличными'},
                    ]},
                    {elem: 'bottom', content: [
                        {elem: 'icon', content: [
                            {block: 'image', mods: {size: '48x48'}, content: [
                                {block: 'img', mods: {lazy: true}, src: './images/payment/wallet.png'},
                            ]},
                        ]},
                    ]},
                    {elem: 'disable', content: [
                        {elem: 'disable-text', content: [
                            {block: 'b', content: 'В разработке,'},
                            {block: 'span', content: 'но совсем скоро появится на сайте'},
                        ]},
                    ]},
                ]},
            ]},
            {elem: 'slide', cls: 'swiper-slide', content: [
                {block: 'square-card', content: [
                    {elem: 'top', content: [
                        {elem: 'title', attrs: {'data-toggle': 'ellipsis'}, content: 'Банковский счёт'},
                    ]},
                    {elem: 'bottom', content: [
                        {elem: 'icon', content: [
                            {block: 'image', mods: {size: '48x48'}, content: [
                                {block: 'img', mods: {lazy: true}, src: './images/payment/bank.png'},
                            ]},
                        ]},
                    ]},
                    {elem: 'disable', content: [
                        {elem: 'disable-text', content: [
                            {block: 'b', content: 'В разработке,'},
                            {block: 'span', content: 'но совсем скоро появится на сайте'},
                        ]},
                    ]},
                ]},
            ]},
            {elem: 'slide', cls: 'swiper-slide', content: [
                {block: 'square-card', mods: {disable: true}, content: [
                    {elem: 'top', content: [
                        {elem: 'title', attrs: {'data-toggle': 'ellipsis'}, content: 'Apple Pay и Samsung Pay'},
                    ]},
                    {elem: 'bottom', content: [
                        {elem: 'icon', content: [
                            {block: 'image', mods: {size: '58x24'}, content: [
                                {block: 'img', mods: {lazy: true}, src: './images/payment/apple-pay.png'},
                            ]},
                        ]},
                    ]},
                    {elem: 'disable', content: [
                        {elem: 'disable-text', content: [
                            {block: 'b', content: 'В разработке,'},
                            {block: 'span', content: 'но совсем скоро появится на сайте'},
                        ]},
                    ]},
                ]},
            ]},
            {elem: 'slide', cls: 'swiper-slide', content: [
                {block: 'square-card', mods: {disable: true}, content: [
                    {elem: 'top', content: [
                        {elem: 'title', attrs: {'data-toggle': 'ellipsis'}, content: 'WebMoney, Яндекс.Деньги, QIWI, PayPal'},
                    ]},
                    {elem: 'bottom', content: [
                        {elem: 'icon', content: [
                            {block: 'image', mods: {size: '48x48'}, content: [
                                {block: 'img', mods: {lazy: true}, src: './images/payment/webmoney.png'},
                            ]},
                        ]},
                    ]},
                    {elem: 'disable', content: [
                        {elem: 'disable-text', content: [
                            {block: 'b', content: 'В разработке,'},
                            {block: 'span', content: 'но совсем скоро появится на сайте'},
                        ]},
                    ]},
                ]},
            ]},
            {elem: 'slide', cls: 'swiper-slide', content: [
                {block: 'square-card', mods: {disable: true}, content: [
                    {elem: 'top', content: [
                        {elem: 'title', attrs: {'data-toggle': 'ellipsis'}, content: 'С баланса телефона'},
                    ]},
                    {elem: 'bottom', content: [
                        {elem: 'icon', content: [
                            {block: 'image', mods: {size: '48x48'}, content: [
                                {block: 'img', mods: {lazy: true}, src: './images/payment/phone.png'},
                            ]},
                        ]},
                    ]},
                    {elem: 'disable', content: [
                        {elem: 'disable-text', content: [
                            {block: 'b', content: 'В разработке,'},
                            {block: 'span', content: 'но совсем скоро появится на сайте'},
                        ]},
                    ]},
                ]},
            ]},
            {elem: 'slide', cls: 'swiper-slide', content: [
                {block: 'square-card', mods: {disable: true}, content: [
                    {elem: 'top', content: [
                        {elem: 'title', attrs: {'data-toggle': 'ellipsis'}, content: 'Вариант оплаты'},
                    ]},
                    {elem: 'bottom', content: [
                        {elem: 'icon', content: [
                            {block: 'image', mods: {size: '48x48'}, content: [
                                {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/48x48'},
                            ]},
                        ]},
                    ]},
                    {elem: 'disable', content: [
                        {elem: 'disable-text', content: [
                            {block: 'b', content: 'В разработке,'},
                            {block: 'span', content: 'но совсем скоро появится на сайте'},
                        ]},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
