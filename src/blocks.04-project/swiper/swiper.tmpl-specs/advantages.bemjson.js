/* eslint-disable linebreak-style */
module.exports = [
    {block: 'slider-advantages', content: [
        {elem: 'inner', cls: 'container', content: [
            {block: 'swiper', cls: 'swiper-advantages', content: [
                {cls: 'swiper-wrapper', content: [
                    {elem: 'slide', cls: 'swiper-slide', content: [
                        {block: 'advantages-card', content: [
                            {elem: 'image', content: [
                                {block: 'image', mods: {size: '96x96'}, cls: 'text-center', content: [
                                    {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/96x96'},
                                ]},
                            ]},
                            {elem: 'dot'},
                            {elem: 'desc', content: 'Доставка по всей России'},
                        ]},
                    ]},
                    {elem: 'slide', cls: 'swiper-slide', content: [
                        {block: 'advantages-card', content: [
                            {elem: 'image', content: [
                                {block: 'image', mods: {size: '96x96'}, cls: 'text-center', content: [
                                    {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/96x96'},
                                ]},
                            ]},
                            {elem: 'dot'},
                            {elem: 'desc', content: 'Команда профессионалов'},
                        ]},
                    ]},
                    {elem: 'slide', cls: 'swiper-slide', content: [
                        {block: 'advantages-card', content: [
                            {elem: 'image', content: [
                                {block: 'image', mods: {size: '96x96'}, cls: 'text-center', content: [
                                    {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/96x96'},
                                ]},
                            ]},
                            {elem: 'dot'},
                            {elem: 'desc', content: 'Собственное производство'},
                        ]},
                    ]},
                    {elem: 'slide', cls: 'swiper-slide', content: [
                        {block: 'advantages-card', content: [
                            {elem: 'image', content: [
                                {block: 'image', mods: {size: '96x96'}, cls: 'text-center', content: [
                                    {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/96x96'},
                                ]},
                            ]},
                            {elem: 'dot'},
                            {elem: 'desc', content: 'Ассортимент товаров в наличии'},
                        ]},
                    ]},
                    {elem: 'slide', cls: 'swiper-slide', content: [
                        {block: 'advantages-card', content: [
                            {elem: 'image', content: [
                                {block: 'image', mods: {size: '96x96'}, cls: 'text-center', content: [
                                    {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/96x96'},
                                ]},
                            ]},
                            {elem: 'dot'},
                            {elem: 'desc', content: 'Скидки на покупку оптом'},
                        ]},
                    ]},
                    {elem: 'slide', cls: 'swiper-slide', content: [
                        {block: 'advantages-card', content: [
                            {elem: 'image', content: [
                                {block: 'image', mods: {size: '96x96'}, cls: 'text-center', content: [
                                    {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/96x96'},
                                ]},
                            ]},
                            {elem: 'dot'},
                            {elem: 'desc', content: 'Комплексный подход к удовлетворению потребностей'},
                        ]},
                    ]},
                    {elem: 'slide', cls: 'swiper-slide', content: [
                        {block: 'advantages-card', content: [
                            {elem: 'image', content: [
                                {block: 'image', mods: {size: '96x96'}, cls: 'text-center', content: [
                                    {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/96x96'},
                                ]},
                            ]},
                            {elem: 'dot'},
                            {elem: 'desc', content: 'Наш вклад в природу'},
                            {block: 'a', mods: {more: true, inherit: true}, content: 'Смотреть'},
                        ]},
                    ]},
                ]},
                {elem: 'line'},
            ]},
            {elem: 'navigation', content: [
                {elem: 'buttons', content: [
                    {elem: 'button', mods: {prev: true}},
                    {elem: 'button', mods: {next: true}},
                ]},
            ]},
        ]},
    ]},
];
