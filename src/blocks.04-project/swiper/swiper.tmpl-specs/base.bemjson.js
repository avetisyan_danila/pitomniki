/* eslint-disable linebreak-style */
module.exports = [
    {block: 'swiper', cls: 'swiper-main', content: [
        {cls: 'swiper-wrapper', content: [
            {elem: 'slide', cls: 'swiper-slide', content: [
                {elem: 'container', cls: 'container', content: [
                    {elem: 'image-wrapper', content: [
                        {elem: 'image', content: [
                            {block: 'image', mods: {size: '448x448'}, cls: 'text-center', content: [
                                {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/448x448'},
                            ]},
                        ]},
                    ]},
                    {elem: 'content', content: [
                        {elem: 'title', content: 'Скидка на садовые ножницы Fiskars до 50%'},
                        {elem: 'desc', content: 'Акция действует до 20.12.2020'},
                        {elem: 'btn', cls: 'btn', content: 'Подробнее'},
                    ]},
                ]},
            ]},
        ]},
        {cls: 'container', content: [
            {elem: 'navigation-wrapper', content: [
                {elem: 'buttons', content: [
                    {elem: 'button', mods: {prev: true}},
                    {elem: 'button', mods: {next: true}},
                ]},
            ]},
        ]},
    ]},
];
