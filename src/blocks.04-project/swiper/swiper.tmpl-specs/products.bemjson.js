/* eslint-disable linebreak-style */
module.exports = [
    {block: 'swiper', cls: 'swiper-cards', content: [
        {cls: 'swiper-wrapper', content: [
            [...new Array(8)].map(() => [
                {elem: 'slide', cls: 'swiper-slide', content: [
                    require('./../../../blocks.04-project/product-card/product-card.tml-specs/forSlider.bemjson'),
                ]},
            ]),
        ]},
    ]},
];
