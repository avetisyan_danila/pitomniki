/* eslint-disable linebreak-style */
module.exports = [
    {block: 'swiper', cls: 'swiper-columns', content: [
        {cls: 'swiper-wrapper', content: [
            [...new Array(12)].map(() => [
                {elem: 'slide', cls: 'swiper-slide', content: [
                    {block: 'client-card', mods: {small: true}, content: [
                        {elem: 'image', content: [
                            {block: 'image', mods: {size: '130x80'}, content: [
                                {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/130x80'},
                            ]},
                        ]},
                    ]},
                ]},
            ]),
        ]},
    ]},
];