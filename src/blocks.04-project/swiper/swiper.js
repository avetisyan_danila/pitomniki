/* eslint-disable linebreak-style */
import {Swiper, Navigation, Grid} from 'swiper';

Swiper.use([Navigation, Grid]);

const swiper = new Swiper('.swiper-main', {
    loop: true,

    navigation: {
        nextEl: '.swiper__button_next',
        prevEl: '.swiper__button_prev',
    },
});

const swiperCards = new Swiper('.swiper-cards', {
    slidesPerView: 4,
    spaceBetween: 30,

    navigation: {
        nextEl: '.tab-panel__button_next',
        prevEl: '.tab-panel__button_prev',
    },
});

const swiperAdvantages = new Swiper('.swiper-advantages', {
    slidesPerView: 6,

    navigation: {
        nextEl: '.slider-advantages__button_next',
        prevEl: '.slider-advantages__button_prev',
    },
});

const swiperPayment = new Swiper('.swiper-payment', {
    slidesPerView: 6,

    navigation: {
        nextEl: '.tab-panel__button_next',
        prevEl: '.tab-panel__button_prev',
    },
});

const swiperColumns = new Swiper('.swiper-columns', {
    slidesPerView: 3,
    grid: {
        rows: 3,
        fill: 'row',
    },

    navigation: {
        nextEl: '.top-columns__button_next',
        prevEl: '.top-columns__button_prev',
    },
});

const swiperPartners = new Swiper('.swiper-partners', {
    slidesPerView: 6,

    navigation: {
        nextEl: '.slider-partners__button_next',
        prevEl: '.slider-partners__button_prev',
    },
});
