/* eslint-disable linebreak-style */
module.exports = [
    {cls: 'col-3', content: [
        {block: 'product-card', content: [
            {elem: 'img', content: [
                {block: 'image', mods: {size: '252x200'}, cls: 'text-center', content: [
                    {block: 'img', mods: {lazy: true}, src: 'http://placehold.it/252x200'},
                ]},
            ]},
            {elem: 'info', content: [
                {elem: 'title', content: 'Бур садовый Fiskars Quikdrill большой'},
                {elem: 'price', content: [
                    {elem: 'price-current', content: '4 717 ₽'},
                    {elem: 'price-old', content: '4 210 ₽'},
                ]},
                {elem: 'cart', content: []},
            ]},
            {block: 'badges', content: [
                {elem: 'item', cls: 'badge badge-info', content: 'Новинка'},
                {elem: 'item', cls: 'badge badge-warning', content: '-25%'},
                {elem: 'item', cls: 'badge badge-danger', mods: {hit: true}, content: 'хит'},
            ]},
        ]},
    ]},
];
