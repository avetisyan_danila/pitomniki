module.exports = {
    mustDeps: [
        {block: 'font-muller'},
        {block: 'fi'},
    ],
    shouldDeps: [
        {block: 'bootstrap'},
    ],
};
