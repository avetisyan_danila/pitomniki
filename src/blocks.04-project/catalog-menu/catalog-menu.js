/* eslint-disable linebreak-style */
const catalogMenuLinks = Array.from(document.querySelectorAll('.catalog-menu__link'));
const catalogMenuPreview = document.querySelector('.catalog-menu__preview');
const catalogMenuPreviewTitle = catalogMenuPreview.querySelector('.catalog-menu__preview-title');
const catalogMenuPreviewDesc = catalogMenuPreview.querySelector('.catalog-menu__preview-desc');

catalogMenuLinks.forEach((link) => {
    link.addEventListener('mouseover', () => {
        catalogMenuPreviewTitle.textContent = link.textContent;
        catalogMenuPreviewDesc.textContent = link.getAttribute('data-desc');
        catalogMenuPreview.style.backgroundImage = link.getAttribute('data-img');
    });
});