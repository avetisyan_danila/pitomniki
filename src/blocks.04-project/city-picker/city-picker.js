/* eslint-disable linebreak-style */
const cityPickerOpenButton = document.querySelector('.user-block__city');
const cityPickerCloseButton = document.querySelector('.city-picker__close');
const cityPickerModal = document.querySelector('.header__city-picker');

cityPickerOpenButton.addEventListener('click', () => {
    cityPickerModal.classList.add('active');
});

cityPickerCloseButton.addEventListener('click', () => {
    cityPickerModal.classList.remove('active');
});
