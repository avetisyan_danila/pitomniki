module.exports = [
  {block: 'header', content: [
      {cls: 'container', content: [
          {elem: 'inner', content: [
              {block: 'a', content: [
                  {block: 'logo', content: [
                      {block: 'image', mods: {size: '180x85'}, content: [
                          {block: 'img', mods: {lazy: true}, src: './images/logo.svg'},
                      ]},
                      {elem: 'title', content: 'Магазин для питомников'},
                  ]},
              ]},
              {elem: 'content', content: [
                  {elem: 'row', content: [
                      {block: 'user-block', content: [
                          {elem: 'list', content: [
                              {elem: 'item', content: [
                                  {elem: 'city', content: [
                                      {block: 'a', content: 'Барнаул'},
                                  ]},
                              ]},
                              {elem: 'item', content: [
                                  {block: 'a', content: 'Частные лица'},
                              ]},
                              {elem: 'item', content: [
                                  {block: 'a', content: 'Корпоративные клиенты'},
                              ]},
                          ]},
                          {elem: 'authorization', content: [
                              {block: 'a', content: 'Регистрация'},
                              {block: 'a', content: 'Войти'},
                          ]},
                      ]},
                  ]},
                  {elem: 'row', content: [
                      {block: 'info', content: [
                          {elem: 'list', content: [
                              {elem: 'item', mods: {search: true}, content: [
                                  {block: 'search', content: [
                                      {block: 'form-control', mix: {block: 'search', elem: 'input', mods: {main: true}}, placeholder: 'Что будем искать?'},
                                      {elem: 'icon', content: []},
                                  ]},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'phone', content: [
                                      {block: 'a', content: '8 (800) 775-38-58'},
                                      {block: 'a', content: 'Перезвоните мне'},
                                  ]},
                              ]},
                          ]},
                          {block: 'a', content: [
                              {block: 'cart', content: [
                                  {elem: 'icon', content: []},
                                  {elem: 'text', content: [
                                      {elem: 'empty', content: 'В корзине нет товаров'},
                                  ]},
                              ]},
                          ]},
                      ]},
                  ]},
              ]},
          ]},
      ]},
      {elem: 'bottom', cls: 'navbar navbar-dark navbar-expand', content: [
          {cls: 'container', content: [
              {block: 'nav', content: [
                  {elem: 'list', content: [
                      {elem: 'item', mods: {catalog: true}, content: [
                          {elem: 'link', content: 'Каталог товаров'},
                          {elem: 'dropdown', mods: {catalog: true}, content: [
                              {block: 'catalog-menu', content: [
                                  {elem: 'row', content: [
                                      {elem: 'col', mods: {catalog: true}, content: [
                                          {elem: 'list', content: [
                                              {elem: 'item', content: [
                                                  {elem: 'link', attrs: {'data-desc': 'Тканный полипропиленовый материал', 'data-img': 'url(./images/products/product-image.jpg)'}, mix: {block: 'dropdown-link'}, content: 'Агроткани'},
                                              ]},
                                              {elem: 'item', content: [
                                                  {elem: 'link', attrs: {'data-desc': '2', 'data-img': 'url(https://via.placeholder.com/150/FF0000/FFFFFF)'}, mix: {block: 'dropdown-link'}, content: 'Агрохимия'},
                                              ]},
                                              {elem: 'item', content: [
                                                  {elem: 'link', attrs: {'data-desc': '3', 'data-img': 'url(https://via.placeholder.com/150/FFFF00/000000)'}, mix: {block: 'dropdown-link'}, content: 'Бамбуковые поддержки'},
                                              ]},
                                              {elem: 'item', content: [
                                                  {elem: 'link', attrs: {'data-desc': '4', 'data-img': 'url(https://via.placeholder.com/150/000000/FFFFFF)'}, mix: {block: 'dropdown-link'}, content: 'Инструмент'},
                                              ]},
                                              {elem: 'item', content: [
                                                  {elem: 'link', attrs: {'data-desc': '5', 'data-img': 'url(https://via.placeholder.com/150/0000FF/808080)'}, mix: {block: 'dropdown-link'}, content: 'Инструмент и техника STIHL/VIKING'},
                                              ]},
                                              {elem: 'item', content: [
                                                  {elem: 'link', attrs: {'data-desc': '6', 'data-img': 'url(https://via.placeholder.com/150/FF0000/FFFFFF)'}, mix: {block: 'dropdown-link'}, content: 'Контейнеры'},
                                              ]},
                                              {elem: 'item', content: [
                                                  {elem: 'link', attrs: {'data-desc': '7', 'data-img': 'url(https://via.placeholder.com/150/FFFF00/000000)'}, mix: {block: 'dropdown-link'}, content: 'Приборы, техника и оборудование'},
                                              ]},
                                              {elem: 'item', content: [
                                                  {elem: 'link', attrs: {'data-desc': '8', 'data-img': 'url(https://via.placeholder.com/150/000000/FFFFFF)'}, mix: {block: 'dropdown-link'}, content: 'Расходные материалы'},
                                              ]},
                                              {elem: 'item', content: [
                                                  {elem: 'link', attrs: {'data-desc': '9', 'data-img': 'url(https://via.placeholder.com/150/0000FF/808080)'}, mix: {block: 'dropdown-link'}, content: 'Семена гозонных трав'},
                                              ]},
                                              {elem: 'item', content: [
                                                  {elem: 'link', attrs: {'data-desc': '10', 'data-img': 'url(https://via.placeholder.com/150/FF0000/FFFFFF)'}, mix: {block: 'dropdown-link'}, content: 'Сетки для упаковки комьев'},
                                              ]},
                                              {elem: 'item', content: [
                                                  {elem: 'link', attrs: {'data-desc': '11', 'data-img': 'url(https://via.placeholder.com/150/FFFF00/000000)'}, mix: {block: 'dropdown-link'}, content: 'Сопутствующие товары'},
                                              ]},
                                          ]},
                                      ]},
                                      {elem: 'col', mods: {preview: true}, content: [
                                          {elem: 'preview', content: [
                                              {elem: 'preview-title', content: 'Агроткани'},
                                              {elem: 'preview-desc', content: 'Тканный полипропиленовый материал'},
                                          ]},
                                      ]},
                                  ]},
                              ]},
                          ]},
                      ]},
                      {elem: 'item', mods: {about: true, dropdown: true}, content: [
                          {elem: 'link', content: 'О компании'},
                          {elem: 'dropdown', mods: {about: true}, content: [
                              {block: 'about-menu', content: [
                                  {elem: 'item', content: [
                                      {elem: 'link', mix: {block: 'dropdown-link'}, content: 'Прайс-листы'},
                                  ]},
                                  {elem: 'item', content: [
                                      {elem: 'link', mix: {block: 'dropdown-link'}, content: 'Доставка'},
                                  ]},
                                  {elem: 'item', content: [
                                      {elem: 'link', mix: {block: 'dropdown-link'}, content: 'Оплата'},
                                  ]},
                                  {elem: 'item', content: [
                                      {elem: 'link', mix: {block: 'dropdown-link'}, content: 'Сервис'},
                                  ]},
                              ]},
                          ]},
                      ]},
                      {elem: 'item', content: [
                          {elem: 'link', content: 'Пресс центр'},
                      ]},
                      {elem: 'item', content: [
                          {elem: 'link', content: 'Акции'},
                      ]},
                      {elem: 'item', content: [
                          {elem: 'link', content: 'Подарочные сертификаты'},
                      ]},
                      {elem: 'item', content: [
                          {elem: 'link', content: 'Новинки'},
                      ]},
                      {elem: 'item', content: [
                          {elem: 'link', content: 'Полезные видео'},
                      ]},
                      {elem: 'item', content: [
                          {elem: 'link', content: 'FAQ'},
                      ]},
                  ]},
              ]},
          ]},
      ]},

      {elem: 'city-picker', content: [
          {block: 'city-picker', content: [
              {elem: 'header', content: [
                  {elem: 'row', content: [
                      {elem: 'col', mods: {title: true}, content: [
                          {elem: 'title', content: 'Выбрать город'},
                      ]},
                      {elem: 'col', mods: {search: true}, content: [
                          {elem: 'search', content: [
                              {block: 'search', content: [
                                  {block: 'form-control', mix: {block: 'search', elem: 'input', mods: {main: true}}, placeholder: 'Что будем искать?'},
                                  {elem: 'icon', content: []},
                              ]},
                          ]},
                      ]},
                      {elem: 'col', mods: {controls: true}, content: [
                          {elem: 'tabs', cls: 'nav', content: [
                              {elem: 'tab', cls: 'active', attrs: {'data-toggle': 'tab', 'role': 'tab', 'href': '#city-picker-tab-1'}, content: 'Алфавит'},
                              {elem: 'tab', attrs: {'data-toggle': 'tab', 'role': 'tab', 'href': '#city-picker-tab-2'}, content: 'Регион'},
                          ]},
                      ]},
                  ]},
                  {elem: 'close', content: [
                      {block: 'image', mods: {size: '16x16'}, content: [
                          {block: 'img', mods: {lazy: true}, src: './images/icons/close.svg'},
                      ]},
                  ]},
              ]},
              {elem: 'body', content: [
                  {elem: 'tab-panel', cls: 'active fade show', attrs: {'role': 'tabpanel', 'id': 'city-picker-tab-1'}, content: [
                      {elem: 'list', content: [
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А первый таб'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                      ]},
                  ]},
                  {elem: 'tab-panel', cls: 'fade', attrs: {'role': 'tabpanel', 'id': 'city-picker-tab-2'}, content: [
                      {elem: 'list', content: [
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А второй таб'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                      ]},
                  ]},
                  {elem: 'tab-panel', cls: 'fade', attrs: {'role': 'tabpanel', 'id': 'city-picker-tab-3'}, content: [
                      {elem: 'list', content: [
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А второй таб'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                          {elem: 'group', content: [
                              {elem: 'name', content: 'А'},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Абакан'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Анадырь'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Астрахань'},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: 'Архангельск'},
                              ]},
                          ]},
                      ]},
                  ]},
              ]},
          ]},
      ]},
  ]},
];
