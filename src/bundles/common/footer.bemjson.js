module.exports = [
  {block: 'footer', content: [
      {elem: 'top', content: [
          {cls: 'container', content: [
              {elem: 'row', content: [
                  {elem: 'col', mods: {logo: true}, content: [
                      {elem: 'logo', content: [
                          {block: 'image', mods: {size: '155x72'}, cls: 'text-center', content: [
                              {block: 'img', mods: {lazy: true}, src: './images/logo-white.svg'},
                          ]},
                      ]},
                      {elem: 'image-link', content: [
                          {block: 'image', mods: {size: '200x48'}, cls: 'text-center', content: [
                              {block: 'img', mods: {lazy: true}, src: './images/pit-portal.png'},
                          ]},
                      ]},
                  ]},
                  {elem: 'col', mods: {nav: true}, content: [
                      {elem: 'nav', content: [
                          {elem: 'nav-col', content: [
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'О компании'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Прайс-лист'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Доставка'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Оплата'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Сервис'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Пресс-центр'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Акции'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Новинки'},
                              ]},
                          ]},
                          {elem: 'nav-col', content: [
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Подарочные сертификаты'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Полезные видео'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Вопросы и ответы (FAQ)'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Частным клиентам'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Корпоративным клиентам'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Вакансии'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Условия возврата'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Обработка ПД'},
                              ]},
                              {elem: 'nav-item', content: [
                                  {elem: 'nav-link', content: 'Контакты'},
                              ]},
                          ]},
                      ]},
                  ]},
                  {elem: 'col', mods: {contacts: true}, content: [
                      {elem: 'socials', content: [
                          {block: 'socials', content: [
                              {elem: 'item', content: [
                                  {elem: 'link', content: [
                                      {block: 'fi', mods: {icon: 'vk'}},
                                  ]},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: [
                                      {block: 'fi', mods: {icon: 'ok'}},
                                  ]},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: [
                                      {block: 'fi', mods: {icon: 'yt'}},
                                  ]},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: [
                                      {block: 'fi', mods: {icon: 'twitter'}},
                                  ]},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: [
                                      {block: 'fi', mods: {icon: 'fb'}},
                                  ]},
                              ]},
                              {elem: 'item', content: [
                                  {elem: 'link', content: [
                                      {block: 'fi', mods: {icon: 'inst'}},
                                  ]},
                              ]},
                          ]},
                      ]},
                      {elem: 'contacts', content: [
                          {elem: 'phone', content: [
                              {block: 'a', content: '8 (800) 775-38-58'},
                          ]},
                          {elem: 'call', content: [
                              {block: 'a', content: 'Перезвоните мне'},
                          ]},
                      ]},
                  ]},
              ]},
          ]},
      ]},
      {elem: 'bottom', content: [
          {cls: 'container', content: [
              {elem: 'row', content: [
                  {elem: 'col', mods: {copyright: true}, content: [
                      {elem: 'copyright', content: [
                          {tag: 'span', content: '© 2020 Магазин для Питомников.'},
                          {tag: 'span', content: 'Все права защищены.'},
                      ]},
                  ]},
                  {elem: 'col', mods: {developer: true}, content: [
                      {elem: 'developer', content: [
                          {tag: 'span', content: 'Разработка сайта — '},
                          {block: 'a', content: [
                              {block: 'image', mods: {size: '120x12'}, cls: 'text-center', content: [
                                  {block: 'img', mods: {lazy: true}, src: './images/developer.png'},
                              ]},
                          ]},
                      ]},
                  ]},
              ]},
          ]},
      ]},
  ]},
  require('./modals.bemjson'),
];
