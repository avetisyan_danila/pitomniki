module.exports = {
    block: 'page',
    title: 'Pitomniki',
    content: [
        require('./common/header.bemjson'),

        {block: 'main', cls: 'p-0', content: [
            {elem: 'section', cls: 'py-0', content: [
                require('./../blocks.04-project/swiper/swiper.tmpl-specs/base.bemjson'),
            ]},
            {elem: 'section', content: [
                {block: 'tab-panel', content: [
                    {elem: 'header', content: [
                        {cls: 'container', content: [
                            {elem: 'row', content: [
                                {elem: 'col', mods: {list: true}, content: [
                                    {block: 'tab-list', cls: 'nav', attrs: {role: 'tab-list'}, content: [
                                        {elem: 'link', cls: 'active', attrs: {'data-toggle': 'tab', 'role': 'tab', 'href': '#tab-panel-1'}, content: 'Спецпредложения'},
                                        {elem: 'link', attrs: {'data-toggle': 'tab', 'role': 'tab', 'href': '#tab-panel-2'}, content: 'Новинки'},
                                        {elem: 'link', attrs: {'data-toggle': 'tab', 'role': 'tab', 'href': '#tab-panel-3'}, content: 'Хиты продаж'},
                                    ]},
                                ]},
                                {elem: 'col', mods: {additional: true}, content: [
                                    {block: 'a', mods: {more: true}, content: 'Все спецпредложения'},
                                ]},
                            ]},
                        ]},
                    ]},
                    {elem: 'body', content: [
                        {cls: 'container', content: [
                            {elem: 'wrapper', cls: 'fade active show', attrs: {'role': 'tabpanel', 'id': 'tab-panel-1'}, content: [
                                require('./../blocks.04-project/swiper/swiper.tmpl-specs/products.bemjson'),
                                {elem: 'navigation', content: [
                                    {elem: 'buttons', content: [
                                        {elem: 'button', mods: {prev: true}},
                                        {elem: 'button', mods: {next: true}},
                                    ]},
                                ]},
                            ]},
                            {elem: 'wrapper', cls: 'fade', attrs: {'role': 'tabpanel', 'id': 'tab-panel-2'}, content: [
                                require('./../blocks.04-project/swiper/swiper.tmpl-specs/products.bemjson'),
                                {elem: 'navigation', content: [
                                    {elem: 'buttons', content: [
                                        {elem: 'button', mods: {prev: true}},
                                        {elem: 'button', mods: {next: true}},
                                    ]},
                                ]},
                            ]},
                            {elem: 'wrapper', cls: 'fade', attrs: {'role': 'tabpanel', 'id': 'tab-panel-3'}, content: [
                                require('./../blocks.04-project/swiper/swiper.tmpl-specs/products.bemjson'),
                                {elem: 'navigation', content: [
                                    {elem: 'buttons', content: [
                                        {elem: 'button', mods: {prev: true}},
                                        {elem: 'button', mods: {next: true}},
                                    ]},
                                ]},
                            ]},
                        ]},
                    ]},
                ]},
            ]},
            {elem: 'section', cls: 'bg-cyan text-white', content: [
                {cls: 'container mb-20 pb-13 pb-md-4 mb-lg-17', content: [
                    {block: 'advantages-top', content: [
                        {block: 'h', size: 2, content: 'О компании'},
                        {elem: 'text', content: 'Агрохимия, садовая техника, агроткани, тележки садовые, контейнеры для растений и прочие товары для садоводов.'},
                        {elem: 'text', content: 'Компания Магазин для Питомников занимается производством и продажей товаров для выращивания растений и уходу за ними. Мы реализуем садовую технику, садовый инструмент, агрохимию и другие товары для садоводов. Начиная с 2010 года мы постоянно развиваемся и расширяем наш ассортимент товаров и повышаем качество наших услуг. Наш ассортимент – более 1600 наименований товаров, которые удовлетворят запросы и любителя «покопаться на грядках», и профессионального озеленителя, и питомниковода.'},
                    ]},
                ]},
                require('./../blocks.04-project/swiper/swiper.tmpl-specs/advantages.bemjson'),
            ]},
            {elem: 'section', content: [
                {block: 'tab-panel', content: [
                    {elem: 'header', content: [
                        {cls: 'container', content: [
                            {elem: 'row', content: [
                                {elem: 'col', mods: {list: true}, content: [
                                    {block: 'tab-list', cls: 'nav', attrs: {role: 'tab-list'}, content: [
                                        {elem: 'link', cls: 'active', attrs: {'data-toggle': 'tab', 'role': 'tab', 'href': '#tab-panel-4'}, content: 'Новости'},
                                        {elem: 'link', attrs: {'data-toggle': 'tab', 'role': 'tab', 'href': '#tab-panel-5'}, content: 'Видео'},
                                    ]},
                                ]},
                                {elem: 'col', mods: {additional: true}, content: [
                                    {block: 'a', mods: {more: true}, content: 'Все новости'},
                                ]},
                            ]},
                        ]},
                    ]},
                    {elem: 'body', content: [
                        {cls: 'container', content: [
                            {elem: 'wrapper', cls: 'fade active show', attrs: {'role': 'tabpanel', 'id': 'tab-panel-4'}, content: [
                                {block: 'news-cards', content: [
                                    {cls: 'container p-0', content: [
                                        {elem: 'wrapper', content: [
                                            [...new Array(5)].map(() => [
                                                {cls: 'col-3', content: [
                                                    {block: 'news-card', content: [
                                                        {block: 'a', content: [
                                                            {block: 'image', mods: {size: '270x180'}, content: [
                                                                {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/270x180'},
                                                            ]},
                                                        ]},
                                                        {elem: 'body', content: [
                                                            {elem: 'date', content: '15.02.2022'},
                                                            {elem: 'title', attrs: {href: '#'}, content: 'График работы весеннего сезона'},
                                                            {elem: 'desc', attrs: {'data-toggle': 'ellipsis'}, content: 'Известно, что в Голландии используют насыпные почвы. При высокой технологии ведения сельского хозяйства там собирают до 100 ц/га пшеницы. Рассказывают, что когда академик-почвовед Константин Дмитриевич Глинка сказал голландцам о том, что у них нет почв, то они ответили ему: «Зато у нас есть урожай!»'},
                                                        ]},
                                                    ]},
                                                ]},
                                            ]),
                                            {cls: 'col-3', content: [
                                                {block: 'news-card', content: [
                                                    {block: 'a', content: [
                                                        {block: 'image', mods: {size: '270x180'}, content: [
                                                            {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/270x180'},
                                                        ]},
                                                        {mix: {block: 'news-card', elem: 'play'}},
                                                    ]},
                                                    {elem: 'body', content: [
                                                        {elem: 'date', content: '15.02.2022'},
                                                        {elem: 'title', attrs: {href: '#'}, content: 'График работы весеннего сезона'},
                                                        {elem: 'desc', attrs: {'data-toggle': 'ellipsis'}, content: 'Известно, что в Голландии используют насыпные почвы. При высокой технологии ведения сельского хозяйства там собирают до 100 ц/га пшеницы. Рассказывают, что когда академик-почвовед Константин Дмитриевич Глинка сказал голландцам о том, что у них нет почв, то они ответили ему: «Зато у нас есть урожай!»'},
                                                    ]},
                                                ]},
                                            ]},
                                        ]},
                                    ]},
                                ]},
                            ]},
                            {elem: 'wrapper', cls: 'fade', attrs: {'role': 'tabpanel', 'id': 'tab-panel-5'}, content: 'Видео'},
                        ]},
                    ]},
                ]},
            ]},
            {elem: 'section', mods: {discount: true}, content: [
                {block: 'tab-panel', content: [
                    {elem: 'header', content: [
                        {cls: 'container', content: [
                            {block: 'h', size: 2, cls: 'text-dark mb-5', content: 'Система скидок'},
                            {elem: 'row', content: [
                                {elem: 'col', mods: {list: true}, content: [
                                    {block: 'tab-list', cls: 'nav', attrs: {role: 'tab-list'}, content: [
                                        {elem: 'link', cls: 'active', attrs: {'data-toggle': 'tab', 'role': 'tab', 'href': '#tab-panel-6'}, content: 'Для корпоративных клиентов'},
                                        {elem: 'link', attrs: {'data-toggle': 'tab', 'role': 'tab', 'href': '#tab-panel-7'}, content: 'Для частных лиц'},
                                    ]},
                                ]},
                            ]},
                        ]},
                    ]},
                    {elem: 'body', content: [
                        {cls: 'container', content: [
                            {elem: 'wrapper', cls: 'fade active show', attrs: {'role': 'tabpanel', 'id': 'tab-panel-6'}, content: [
                                {block: 'discount', content: [
                                    {elem: 'row', cls: 'row mb-5', content: [
                                        {elem: 'col', mods: {desc: true}, content: [
                                            {elem: 'desc', content: 'Мы обеспечиваем комплексный подход в удовлетворении ваших потребностей – от планирования закупок до агрономической поддержки при использовании нашей продукции. Чтобы вы могли приобрести всё необходимое в одном месте, мы готовы под заказ привезти товар, не представленный в нашем основном ассортимент.'},
                                        ]},
                                        {elem: 'col', mods: {image: true}, content: [
                                            {elem: 'image', content: [
                                                {block: 'image', mods: {size: '458x503'}, content: [
                                                    {block: 'img', mods: {lazy: true}, src: './images/discount/man-1.png'},
                                                ]},
                                            ]},
                                        ]},
                                    ]},
                                    {elem: 'row', cls: 'row', content: [
                                        {elem: 'col', mods: {range: true}, content: [
                                            {block: 'range', content: [
                                                {elem: 'wrapper', content: [
                                                    {elem: 'inner', content: [
                                                        {elem: 'track-wrapper', content: [
                                                            {elem: 'track', content: [
                                                                {elem: 'fill'},
                                                            ]},
                                                        ]},
                                                        {elem: 'list', content: [
                                                            {elem: 'item', content: [
                                                                {elem: 'point', mods: {active: true}, content: [
                                                                    {elem: 'desc', content: 'Планирование закупок'},
                                                                ]},
                                                            ]},
                                                            {elem: 'item', content: [
                                                                {elem: 'point', content: [
                                                                    {elem: 'desc', content: 'Подготовка коммерческого предложения'},
                                                                ]},
                                                            ]},
                                                            {elem: 'item', content: [
                                                                {elem: 'point', content: [
                                                                    {elem: 'desc', content: 'Обеспечение заказа'},
                                                                ]},
                                                            ]},
                                                            {elem: 'item', content: [
                                                                {elem: 'point', content: [
                                                                    {elem: 'desc', content: 'Постпродажная агрономическая поддержка'},
                                                                ]},
                                                            ]},
                                                        ]},
                                                    ]},
                                                ]},
                                            ]},
                                        ]},
                                    ]},
                                ]},
                            ]},
                            {elem: 'wrapper', cls: 'fade', attrs: {'role': 'tabpanel', 'id': 'tab-panel-7'}, content: [
                                {block: 'discount', content: [
                                    {elem: 'row', cls: 'row mb-5 pb-2', content: [
                                        {elem: 'col', mods: {desc: true}, content: [
                                            {elem: 'row', cls: 'row', content: [
                                                {elem: 'col', mods: {text: true}, content: [
                                                    {elem: 'desc', content: 'Для частных лиц действует накопительная система скидок. Чем больше объем покупок тем выше величина предоставляемой скидки.'},
                                                ]},
                                                {elem: 'col', mods: {additional: true}, content: [
                                                    {elem: 'count', attrs: {'data-text': 'Ваша скидка'}, content: [
                                                        {block: 'span', content: [
                                                            '8',
                                                            {block: 'small', content: '%'},
                                                        ]},
                                                    ]},
                                                ]},
                                            ]},
                                        ]},
                                        {elem: 'col', mods: {image: true}, content: [
                                            {elem: 'image', content: [
                                                {block: 'image', mods: {size: '458x503'}, content: [
                                                    {block: 'img', mods: {lazy: true}, src: './images/discount/man-2.png'},
                                                ]},
                                            ]},
                                        ]},
                                    ]},
                                    {elem: 'row', cls: 'row', content: [
                                        {elem: 'col', mods: {range: true}, content: [
                                            {block: 'range', mods: {steps: true}, content: [
                                                {elem: 'wrapper', content: [
                                                    {elem: 'inner', content: [
                                                        {elem: 'track-wrapper', content: [
                                                            {elem: 'track', content: [
                                                                {elem: 'fill'},
                                                            ]},
                                                        ]},
                                                        {elem: 'list', content: [
                                                            {elem: 'item', attrs: {style: 'width: 36%'}, content: [
                                                                {elem: 'point', content: [
                                                                    {elem: 'desc', content: '100000'},
                                                                ]},
                                                            ]},
                                                            {elem: 'item', attrs: {style: 'width: 23.5%'}, content: [
                                                                {elem: 'point', content: [
                                                                    {elem: 'desc', content: '250000'},
                                                                ]},
                                                            ]},
                                                            {elem: 'item', attrs: {style: 'width: 39%'}, content: [
                                                                {elem: 'point', content: [
                                                                    {elem: 'desc', content: '500000'},
                                                                ]},
                                                            ]},
                                                        ]},
                                                    ]},
                                                ]},
                                            ]},
                                        ]},
                                    ]},
                                ]},
                            ]},
                        ]},
                    ]},
                ]},
            ]},
            {elem: 'section', content: [
                {cls: 'mb-5 pb-2', content: [
                    {block: 'tab-panel', content: [
                        {elem: 'header', content: [
                            {cls: 'container', content: [
                                {elem: 'row', content: [
                                    {elem: 'col', mods: {list: true}, content: [
                                        {block: 'tab-list', cls: 'nav', attrs: {role: 'tab-list'}, content: [
                                            {elem: 'link', cls: 'active', attrs: {'data-toggle': 'tab', 'role': 'tab', 'href': '#tab-panel-8'}, content: 'Оплата'},
                                            {elem: 'link', attrs: {'data-toggle': 'tab', 'role': 'tab', 'href': '#tab-panel-9'}, content: 'Доставка'},
                                        ]},
                                    ]},
                                ]},
                            ]},
                        ]},
                        {elem: 'body', content: [
                            {cls: 'container', content: [
                                {elem: 'wrapper', cls: 'fade active show', attrs: {'role': 'tabpanel', 'id': 'tab-panel-8'}, content: [
                                    require('./../blocks.04-project/swiper/swiper.tmpl-specs/payment.bemjson'),
                                    {elem: 'navigation', content: [
                                        {elem: 'buttons', content: [
                                            {elem: 'button', mods: {prev: true}},
                                            {elem: 'button', mods: {next: true}},
                                        ]},
                                    ]},
                                ]},
                                {elem: 'wrapper', cls: 'fade', attrs: {'role': 'tabpanel', 'id': 'tab-panel-9'}, content: [
                                    require('./../blocks.04-project/swiper/swiper.tmpl-specs/payment.bemjson'),
                                    {elem: 'navigation', content: [
                                        {elem: 'buttons', content: [
                                            {elem: 'button', mods: {prev: true}},
                                            {elem: 'button', mods: {next: true}},
                                        ]},
                                    ]},
                                ]},
                            ]},
                        ]},
                    ]},
                ]},
                {cls: 'container', content: [
                    {cls: 'row', content: [
                        {cls: 'col-7', content: [
                            {block: 'h', size: 2, cls: 'mb-4', content: 'Регион работы'},
                            {block: 'image', mods: {size: '700x320'}, content: [
                                {block: 'img', mods: {lazy: true}, src: 'http://place-hold.it/700x320'},
                            ]},
                        ]},
                        {cls: 'col-5', content: [
                            {block: 'h', size: 2, cls: 'mb-4', content: 'ТОП 30 клиентов'},
                            {block: 'top-columns', content: [
                                require('./../blocks.04-project/swiper/swiper.tmpl-specs/columns.bemjson'),
                                {elem: 'navigation', content: [
                                    {elem: 'buttons', content: [
                                        {elem: 'button', mods: {prev: true}},
                                        {elem: 'button', mods: {next: true}},
                                    ]},
                                ]},
                            ]},
                        ]},
                    ]},
                ]},
            ]},
            {elem: 'section', cls: 'bg-light', content: [
                {cls: 'container', content: [
                    {block: 'h', size: 2, cls: 'mb-4', content: 'Партнеры'},
                    require('./../blocks.04-project/swiper/swiper.tmpl-specs/partners.bemjson'),
                ]},
            ]},
        ]},

        require('./common/footer.bemjson'),
    ],
};
